import { has } from 'lodash';

const utils = {
    storage: {
        get: (key) => {
            return localStorage.getItem(key);
        },
        set: (key, value) => {
            localStorage.setItem(key, value);
        },
        delete: (key) => {
            localStorage.removeItem(key);
        },
        clear: () => {
            localStorage.clear();
        }
    },
    env: {
        get: (key) => {
            return has(process.env, key) ? process.env[key] : null;
        }
    }
}

export default utils;