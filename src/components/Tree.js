import React, { useState } from 'react';
import FontAwesome from 'react-fontawesome';
import { has, isEmpty } from 'lodash';

const Tree = ({ nodes = [], index = null }) => {
    return (
        <ul className='d-flex flex-column list-style'>
            {!isEmpty(nodes) && nodes.map(node => (
                <TreeNode node={node} index={index} />
            ))}
        </ul>
    )
}

const TreeNode = ({ node, index }) => {

    const [childVisible, setChildVisibility] = useState(false);

    const hasChild = has(node, 'children') && !isEmpty(node.children) ? true : false;
   
    let fontIcon = 'angle-right';
    if (childVisible) {
        fontIcon = 'angle-down';
    }

    const handleChange = (e) => {
        setChildVisibility(v => !v);
    }
    
    return (     
        <>
            {index === 0 &&
                (<li>
                    <input
                        className='ms-3 me-2'
                        type="checkbox"
                        onChange=''
                    />
                    select all
                </li>)
            }
            <li key={node.id}>
                {hasChild ?
                    (<FontAwesome
                        className="super-crazy-colors me-2"
                        name={fontIcon}
                        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
                    />) : (
                        <span className='me-3' aria-hidden="true"></span>
                    )
                }
                <input
                    className='me-2'
                    type="checkbox"
                    value={node.id}
                    onChange={handleChange}
                />
                {node.name}
                {hasChild && childVisible && node.children.map((children, index) => (
                    <Tree nodes={[children]} index={index} />
                ))}
            </li>
        </>
    )
}

export default Tree