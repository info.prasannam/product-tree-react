import { Outlet } from 'react-router-dom';

const Layout = () => {
    return (
        <div className='container'>
            <div className='row'>
                <div className="col-12">
                    <Outlet />
                </div>
            </div>
        </div>
    )
}

export default Layout