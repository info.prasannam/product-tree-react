import React, { Component } from 'react';
import axios from 'axios';
import Tree from './Tree';
import utils from '../utils';

class Category extends Component {

    constructor() {
        super();
        this.getCategories();
    }

    state = {
        categories: [],
        errorMessage: ''
    }
    
    getCategories = async () => {
        try {
            await axios({
                url: utils.env.get('REACT_APP_API_URL') + '/category',
                method: 'get',
                headers: {
                    Authorization: 'Bearer ' + utils.storage.get('token') || '',
                    'Content-Type': 'application/json' 
                }
            }).then((response) => {
                this.setState({ categories: response.data });
            }).catch((error) => {
                let errorMessage = 'An error occured';
                if (error.response.data.message) {
                    errorMessage = error.response.data.message;
                }
                this.setState({ errorMessage });
            });
        } catch (error) {
            this.setState({ errorMessage: error });
        }
    }

    render () {
        return (
            <>
                <div className="row">
                    <div className="col-4"></div>
                    <div className="col-8">
                        <h3>Category List</h3>
                        {this.state.errorMessage && (<div className='alert-danger text-center'>{this.state.errorMessage}</div>)}
                        <Tree nodes={this.state.categories} />
                    </div>
                </div>
            </>
        )
    }
}

export default Category