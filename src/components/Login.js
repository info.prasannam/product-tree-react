import { useRef, useState, useEffect, useContext } from 'react';
import utils from '../utils';
import axios from 'axios';
import useAuth from '../hooks/useAuth';
import { useNavigate, useLocation } from 'react-router-dom';

const Login = () => {

    const { setAuth } = useAuth();

    const navigate = useNavigate();
    const location = useLocation();
    const from = location.state?.from?.pathname || '/';

    const userRef = useRef();
    const errRef = useRef();

    const [user, setUser] = useState('');
    const [pwd, setPwd] = useState('');
    const [errMsg, setErrMsg] = useState('');

    useEffect(() => {
        userRef.current.focus();
    }, []);

    useEffect(() => {
        setErrMsg('');
    }, [user, pwd])

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await axios({
                url: utils.env.get('REACT_APP_API_URL') + '/login',
                method: 'post',
                data: JSON.stringify({
                    email: user,
                    password: pwd
                }),
                headers: {
                   'Content-Type': 'application/json' 
                }
            }).then((response) => {
                const accessToken = response?.data?.accessToken;
                utils.storage.set('token', accessToken);
                setAuth({ user, accessToken })
                navigate(from, { replace: true });
            }).catch((error) => {
                let errorMessage = 'An error occured';
                if (error?.response?.data?.message) {
                    errorMessage = error?.response?.data?.message;
                }
                setErrMsg(errorMessage);
            });
        } catch (error) {
            setErrMsg(error);
        }
    }

    return (
        <div className='container row'>
            <div className='col-4'></div>
            <div className='col-4'>
                <h1>Login Form</h1>
                <form onSubmit={handleSubmit}>
                    <div className='form-group mt-5'>
                        <label htmlFor='username'>Username</label>
                        <input
                            className='form-control'
                            type='text'
                            id='username'
                            ref={userRef}
                            autoComplete='off'
                            onChange={(e) => setUser(e.target.value)}
                            value={user}
                            required
                        />
                    </div>
                    <div className='form-group mt-3'>
                        <label htmlFor='password'>Password</label>
                        <input
                            className='form-control'
                            type='password'
                            id='password'
                            autoComplete='off'
                            onChange={(e) => setPwd(e.target.value)}
                            value={pwd}
                            required
                        />
                    </div>
                    <div className='form-group mt-3'>
                        <p ref={errRef} className={errMsg ? 'alert-danger text-center' : ''} aria-live={'assertive'} >
                            {errMsg}
                        </p>
                    </div>
                    <div className='form-group mt-3'>
                        <button className='btn btn-primary form-control'>Login</button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Login