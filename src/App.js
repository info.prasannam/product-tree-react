import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from './components/Layout';
import Category from './components/Category';
import Login from './components/Login';
import { Routes, Route } from 'react-router-dom';
import RequireAuth from './components/RequireAuth';

function App() {
    return (
        <Routes>
            <Route path='/' element={<Layout />}>
                <Route path='login' element={<Login />} />
                <Route element={<RequireAuth />}>
                    <Route path='/' element={<Category />} />
                </Route>
            </Route>
        </Routes>
    )
}

export default App;